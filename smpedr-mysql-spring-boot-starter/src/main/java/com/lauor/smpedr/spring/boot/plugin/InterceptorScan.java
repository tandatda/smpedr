package com.lauor.smpedr.spring.boot.plugin;

import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @time:2022/8/5
 * @author 13949
 * @function:自动扫描配置拦截器
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(InterceptorScanRegistrar.class)
public @interface InterceptorScan {

    @AliasFor("basePackages")
    String[] value() default {};

    /**
     * @description:需要扫描的包
     * @date 16:48 2022/8/5
     * @return java.lang.String[]
     */
    @AliasFor("value")
    String[] basePackages() default {};
}
