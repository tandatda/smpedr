package com.lauor.smpedr.plugin;

import java.lang.reflect.Method;

/**
 *所拦截方法信息，用在sql拦截器
 */
public class Invocation {
    private Method method;

    private Object[] args;

    public Invocation(Method method, Object[] args) {
        this.method = method;
        this.args = args;
    }

    public Invocation(Method method) {
        this.method = method;
    }

    public Method getMethod() {
        return method;
    }

    public Object[] getArgs() {
        return args;
    }
}
