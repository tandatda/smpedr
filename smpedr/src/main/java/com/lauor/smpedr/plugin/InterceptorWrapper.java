package com.lauor.smpedr.plugin;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @time:2022/8/6
 * @author:tan
 * @function:拦截器包装
 */
public class InterceptorWrapper {
    //元数据
    private Interceptor meta;
    //拦截的目标方法
    private Set<String> targetMethods;
    //拦截目标对象
    private List<InterceptorScopeEnum> scopes;

    public InterceptorWrapper(Interceptor meta) {
        this.meta = meta;
        //初始化扩展信息
        initExtensions();
    }
    //初始化扩展信息
    private void initExtensions(){
        //获取注解
        Intercepts intercepts = meta.getClass().getAnnotation(Intercepts.class);
        if (intercepts == null) {
            return;
        }
        String[] targets = intercepts.targetMethods();
        this.targetMethods = Stream.of(targets).collect(Collectors.toSet());
        this.scopes = Stream.of( intercepts.scope() ).collect(Collectors.toList());
    }

    public Interceptor getMeta() {
        return meta;
    }

    public Optional< Set<String> > getTargetMethods() {
        return Optional.of(targetMethods);
    }

    public Optional< List<InterceptorScopeEnum> > getScopes() {
        return Optional.of(scopes);
    }
}