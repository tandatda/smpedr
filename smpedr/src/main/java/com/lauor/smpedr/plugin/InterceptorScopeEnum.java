package com.lauor.smpedr.plugin;

/**
 * 拦截器作用范围
 */
public enum InterceptorScopeEnum {
    //拦截sqlSession
    SQL_SESSION,
    //拦截sql executor
    EXECUTOR
}
