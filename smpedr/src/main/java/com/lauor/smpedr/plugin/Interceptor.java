package com.lauor.smpedr.plugin;

/**
 * 拦截器
 */
public interface Interceptor {
    /**
     * 目标方法执行前拦截
     * @param invocation 所拦截方法信息
     * @return boolean 是否继续执行
     */
    boolean before(Invocation invocation);
    /**
     * 目标方法执行后拦截
     * @param invocation 所拦截方法信息
     */
    void after(Invocation invocation);
}
