package com.lauor.smpedr.plugin;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @time:2022/8/5
 * @function:声明为拦截器
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Intercepts {
    /**
     * @description:拦截目标，默认为sql session
     * @see com.lauor.smpedr.session.SqlSession
     * @see com.lauor.smpedr.core.executor.Executor
     * @date 16:34 2022/8/5
     */
    InterceptorScopeEnum[] scope() default InterceptorScopeEnum.SQL_SESSION;
    /**
     * @description:拦截的方法名 SqlSession/Executor
     * @see com.lauor.smpedr.session.SqlSession
     * @see com.lauor.smpedr.core.executor.Executor
     * @date 14:53 2022/7/24
     * @return java.util.List<java.lang.String>
     */
    String[] targetMethods();
}
