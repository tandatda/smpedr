package com.lauor.smpedr;

import com.lauor.smpedr.core.handler.TypeHandler;
import com.lauor.smpedr.core.handler.provider.DefaultTypeHandler;
import com.lauor.smpedr.plugin.Interceptor;
import com.lauor.smpedr.plugin.InterceptorWrapper;
import com.lauor.smpedr.transaction.TransactionFactory;

import javax.sql.DataSource;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 配置类
 */
public class Configuration {
    //key:methodName
    private final Map<String, MethodSignature> METHOD_CACHE = new ConcurrentHashMap<>();
    //拦截器链
    private final List<InterceptorWrapper> INTERCEPTOR_List = new LinkedList<>();
    //数据连接池
    private DataSource dataSource;
    //事务工厂
    private TransactionFactory transactionFactory;
    //类型转换器
    private TypeHandler typeHandler;

    public Configuration(TransactionFactory transactionFactory) {
        this(null, transactionFactory);
    }

    public Configuration(DataSource dataSource, TransactionFactory transactionFactory) {
        this.dataSource = dataSource;
        this.transactionFactory = transactionFactory;
        this.typeHandler = new DefaultTypeHandler();
    }

    public void addMethodSignature(String methodName, MethodSignature methodSignature){
        METHOD_CACHE.put(methodName, methodSignature);
    }

    public void addInterceptor(Interceptor interceptor){
        this.INTERCEPTOR_List.add( new InterceptorWrapper(interceptor) );
    }

    public List<InterceptorWrapper> getInterceptorList() {
        return INTERCEPTOR_List;
    }

    public MethodSignature getMethodSignature(String methodName){
        return METHOD_CACHE.get(methodName);
    }

    public TransactionFactory getTransactionFactory(){
        return this.transactionFactory;
    }

    public DataSource getDataSource(){
        return this.dataSource;
    }

    public TypeHandler getTypeHandler() {
        return typeHandler;
    }

    public void setTypeHandler(TypeHandler typeHandler) {
        this.typeHandler = typeHandler;
    }
}
