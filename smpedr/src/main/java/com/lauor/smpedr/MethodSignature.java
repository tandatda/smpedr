package com.lauor.smpedr;

import java.lang.reflect.Method;
import java.util.List;

/**
 * 方法信息
 */
public class MethodSignature {
    private final boolean returnsList;
//    private final boolean returnArray;//返回数组（Object[]）
//    private final boolean returnsMap;
    private final boolean returnsVoid;
    private final Class<?> returnType;

    public MethodSignature(Method method) {
        this.returnType = method.getReturnType();
        this.returnsList = List.class.isAssignableFrom(this.returnType);
//        this.returnArray = this.returnType.isArray();
//        this.returnsMap = Map.class.isAssignableFrom(this.returnType);
        this.returnsVoid = void.class.equals(this.returnType);
    }

    public boolean isReturnsList() {
        return returnsList;
    }

    public boolean isReturnsVoid() {
        return returnsVoid;
    }

    public Class<?> getReturnType() {
        return returnType;
    }
}
