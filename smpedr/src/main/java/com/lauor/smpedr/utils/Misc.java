package com.lauor.smpedr.utils;

/**
 * 工具类
 */
public class Misc {
    /**parse begin*/
    public static Long parserLong(Object val, long defaultVal){
        try {
            return Long.parseLong( String.valueOf(val) );
        } catch (Exception e){
            return defaultVal;
        }
    }
    /**parse end*/
}
