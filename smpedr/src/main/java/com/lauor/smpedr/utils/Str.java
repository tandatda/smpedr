package com.lauor.smpedr.utils;

/**
 * string utils
 */
public class Str {

    public static boolean isEmpty(String row){
        return row == null || "".equals( row.trim() );
    }

    public static boolean isNull(String row){
        return row == null || "".equals(row);
    }

    public static String lowerCaseConvert(String content, int index){
        return caseConvert(content, index, false);
    }

    public static String upperCaseConvert(String content, int index){
        return caseConvert(content, index, true);
    }
    /**
     * @description:字符串某字符大小写转换
     * @param content
     * @param index
     * @param toUppercase
     * @return java.lang.String
     */
    public static String caseConvert(String content, int index, boolean toUppercase){
        if (Str.isEmpty(content)) return content;

        char[] charArr = content.toCharArray();
        if (toUppercase){
            if (charArr[index] >= 97 && charArr[index] <= 122){
                charArr[index] -= 32;
            }
        } else {
            if (charArr[index] >= 65 && charArr[index] <= 90){
                charArr[index] += 32;
            }
        }
        return String.valueOf(charArr);
    }
}
