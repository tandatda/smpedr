package com.lauor.smpedr.transaction;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 事务接口
 */
public interface Transaction {

    Connection getConnection() throws SQLException;

    void commit() throws SQLException;

    void rollback() throws SQLException;

    void close() throws SQLException;
    //获取sql超时时间
    Integer getTimeout();
    /**
     * @description:连接是否已关闭
     * @return boolean
     */
    boolean isClosed();
}
