package com.lauor.smpedr.transaction.jdbc;

import com.lauor.smpedr.transaction.Transaction;
import com.lauor.smpedr.transaction.TransactionFactory;
import com.lauor.smpedr.transaction.TransactionIsolationLevel;

import javax.sql.DataSource;
import java.sql.Connection;

/**
 * jdbc事务工厂
 */
public class JdbcTransactionFactory implements TransactionFactory {

    @Override
    public Transaction newTransaction(Connection connection) {
        return new JdbcTransaction(connection);
    }

    @Override
    public Transaction newTransaction(DataSource dataSource, TransactionIsolationLevel level, boolean autoCommit) {
        return new JdbcTransaction(dataSource, level, autoCommit);
    }
}
