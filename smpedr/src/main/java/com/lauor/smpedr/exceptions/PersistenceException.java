package com.lauor.smpedr.exceptions;

/**
 * 持久化异常
 */
public class PersistenceException extends RuntimeException {
    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }
}
