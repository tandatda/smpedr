package com.lauor.smpedr.exceptions;

/**
 * 数据查询异常
 */
public class QueryException extends RuntimeException {
    public QueryException(String message) {
        super(message);
    }

    public QueryException(String message, Throwable cause) {
        super(message, cause);
    }
}
