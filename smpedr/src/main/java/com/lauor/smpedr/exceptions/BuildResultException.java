package com.lauor.smpedr.exceptions;

/**
 * 构建SQL结果异常
 */
public class BuildResultException extends RuntimeException {

    public BuildResultException(String message, Throwable cause) {
        super(message, cause);
    }
}
