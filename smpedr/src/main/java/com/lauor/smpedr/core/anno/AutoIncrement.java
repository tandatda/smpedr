package com.lauor.smpedr.core.anno;

import java.lang.annotation.*;

/**
 * @description 是否为自增字段，默认true
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.ANNOTATION_TYPE, ElementType.FIELD})
public @interface AutoIncrement {
}
