package com.lauor.smpedr.core.anno;

import java.lang.annotation.*;

/**
 * 主键 注解
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Id {
    /**
     * @description:是否为自增主键
     * @return boolean
     * @see com.lauor.smpedr.core.anno.AutoIncrement
     */
    boolean autoIncrement() default true;
    /**
     * @description:更新是否忽略此字段
     * @return boolean true: 更新时候不更新目标字段
     * @see com.lauor.smpedr.core.anno.UpdateIgnore
     */
    boolean updateIgnore() default true;
}
