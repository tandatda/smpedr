package com.lauor.smpedr.core.handler;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * orm类型转换
 */
public interface TypeHandler {
    /**
     * @description:从结果集获取指定的java类型
     * @param resultSet
     * @param columnIndex
     * @param javaBeanType
     * @exception SQLException
     * @return java.lang.Object
     */
    Object jdbcToJavaBeanType(ResultSet resultSet, int columnIndex, Class javaBeanType) throws SQLException;
}
