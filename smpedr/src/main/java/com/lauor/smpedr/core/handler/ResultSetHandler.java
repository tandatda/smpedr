package com.lauor.smpedr.core.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * 处理结果集
 */
public interface ResultSetHandler {
    /**
     * @description:结果集转数组
     * @param resultSet 结果集
     * @param clazz
     * @exception SQLException
     * @return java.util.List<T> not null
     */
    <E> List<E> handleResultSetsForMany(ResultSet resultSet, Class<E> clazz) throws SQLException;
    /**
     * @description:处理返回单个数据
     * @param resultSet
     * * @param clazz
     * @exception SQLException
     * @return E nullable
     */
    <E> E handleResultSetsForSingle(ResultSet resultSet, Class<E> clazz) throws SQLException;
    /**
     * @description:将自增列的值写回对象
     * @param resultSet
     * @param dataList
     * @param cls
     * @exception SQLException
     * @return java.util.List<E>
     */
    <E> List<E> handleGeneratedKeys(ResultSet resultSet, List<E> dataList, Class cls) throws SQLException;
}
