package com.lauor.smpedr.core.executor;

import com.lauor.smpedr.Configuration;
import com.lauor.smpedr.MethodSignature;
import com.lauor.smpedr.param.SqlArgMap;

import java.util.List;

/**
 * @description:sql执行接口
 */
public interface Executor {
    /**
     * @description:执行sql查询语句
     * @param sql sql查询语句，语法: select * from table_name where name = #{param1} and age = #{param2};
     * @param sqlArgs where条件, example: {'param1': 'wangqi', 'param2': 18}
     * @param clazz result class
     * @param invokerSignature 调用者方法信息
     * @exception Exception
     * @return java.util.List<E>
     */
    <E> List<E> query(String sql, SqlArgMap sqlArgs, Class<E> clazz, MethodSignature invokerSignature) throws Exception;
    /**
     * @description:执行更改数据库操作，如:delete update, insert
     * @param sql
     * @param sqlArgs
     * @exception Exception
     * @return int
     */
    int update(String sql, Object obj, SqlArgMap sqlArgs) throws Exception;
    /**
     * @description:执行批量sql
     * @param sql
     * @param dataList
     * @param cls table对应class
     * @exception Exception
     * @return int
     */
    <E> int executeBatch(String sql, List<E> dataList, Class cls) throws Exception;
    /**
     * @description:执行删除
     * @date 15:10 2022/6/2
     * @param sql
     * @param sqlArgs 参数对象
     * @exception Exception
     * @return int
     */
    int delete(String sql, SqlArgMap sqlArgs) throws Exception;

    void commit(boolean force) throws Exception;

    void commit() throws Exception;

    void rollback(boolean force) throws Exception;

    void rollback() throws Exception;

    void close(boolean forceRollback) throws Exception;
    /**
     * @description:连接是否已关闭
     * @return boolean
     */
    boolean isClosed();

    Configuration getConfiguration();
}