package com.lauor.smpedr.core.anno;

import java.lang.annotation.*;

/**
 * @function:插入忽略的属性
 */
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface InsertIgnore {
}
