package com.lauor.smpedr.core;

import com.lauor.smpedr.param.SqlArgMap;
import com.lauor.smpedr.param.SqlParam;

import java.util.List;

/**
 * sql 生成器
 */
public interface SqlGenerator {
    /**
     * @description:生成sql查询语句
     * @param tableName 数据库表名
     * @param columns 所查询的列，默认为*，nullable
     * @param sqlArgMap 查询参数,nullable
     * @param sqlParam limit等
     * @return java.lang.String
     */
    String generateQuery(String tableName, List<String> columns, SqlArgMap sqlArgMap, SqlParam sqlParam);

    String generateQuery(String tableName, SqlArgMap sqlArgs, SqlParam sqlParam);
    //查总数sql生成器
    String generateCount(String tableName, SqlArgMap sqlArgs);

    String generateBatchInsert(Class clazz, String tableName, List dataList, boolean insertNull);

    String generateUpdate(String tableName, Object obj, SqlArgMap sqlArgs, SqlParam sqlParam, boolean updateNull);

    String generateDelete(String tableName, SqlArgMap sqlArgs, SqlParam sqlParam);
    //生成sql where开始部分，不加;号结尾
    String generateWhereSuffix(SqlArgMap sqlArgs, SqlParam sqlParam);
}
