package com.lauor.smpedr.core.helper;

import com.lauor.smpedr.utils.Str;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * orm帮助类
 */
public class OrmHelper {
    /**
     * @description:方法是否为javaBean getter方法
     * @param method
     * @return boolean
     */
    public static boolean isJavaBeanGetterMethod(Method method){
        if (method == null || method.getParameterCount() > 0 || "void".equals( method.getReturnType().getName() )) return false;

        String methodName = method.getName();
        if ("getClass".equals(methodName)) return false;

        boolean isReturnBoolean = "boolean".equals( method.getReturnType().getName() );
        if (methodName.matches("^(get[A-Z])[a-zA-Z0-9_$]+") && !isReturnBoolean
                || methodName.matches("^(is[A-Z])[a-zA-Z0-9_$]+") && isReturnBoolean){
            return true;
        }
        return false;
    }
    /**
     * @description:方法是否为javaBean Setter方法
     * @param method
     * @return boolean
     */
    public static boolean isJavaBeanSetterMethod(Method method){
        if (method == null || method.getParameterCount() == 0 || !"void".equals( method.getReturnType().getName() )) return false;

        String methodName = method.getName();
        return methodName.matches("^(set[A-Z])[a-zA-Z0-9_$]+");
    }
    /**
     * @description:通过javaBean setter法获取对应的属性名
     * @param javaBeanMethod
     * @return java.lang.String
     */
    public static String getFieldBySetterMethod(Method javaBeanMethod){
        if ( !isJavaBeanSetterMethod(javaBeanMethod) ) return "";

        return getFieldByMethod(new Field[0], javaBeanMethod, "set", false);
    }
    /**
     * @description:通过通过javaBean getter法获取对应的属性名
     * @param javaBeanMethod
     * @return java.lang.String
     */
    public static String getFieldByGetterMethod(Method javaBeanMethod){
        return getFieldByGetterMethod(new Field[0], javaBeanMethod, false);
    }
    /**
     * @description:通过javaBean方法获取对应的属性名
     * @param fields 类的属性
     * @param method
     * @param checkField 是否检查通过方法得到的属性是否存在，默true
     * @return java.lang.String
     */
    public static String getFieldByGetterMethod(Field[] fields, Method method, boolean checkField){
        if ( !isJavaBeanGetterMethod(method) ) return "";

        String res = getFieldByMethod(fields, method, "get", checkField);
        return Str.isEmpty(res) ? getFieldByMethod(fields, method, "is", checkField) : res;
    }

    public static String getFieldByGetterMethod(Field[] fields, Method method){
        return getFieldByGetterMethod(fields, method, true);
    }
    /**
     * @description:通过方法名获取其对应的属性值
     * @param fields
     * @param method 要检验的方法
     * @param methodNamePrefix 方法前缀
     * @param checkField 是否检测方法对应的属性是否存在
     * @return boolean
     */
    private static String getFieldByMethod(Field[] fields, Method method, String methodNamePrefix, boolean checkField){
        String fieldName = method.getName().replaceFirst(methodNamePrefix, "");
        if (!checkField) return Str.lowerCaseConvert(fieldName, 0);

        //首字母小写
        fieldName = Str.lowerCaseConvert(fieldName, 0);
        for (Field field : fields) {
            if (fieldName.equals( field.getName() )){
                return fieldName;
            }
        }
        return "";
    }
    /**
     * @description:查询orm getter方法，通过属性名
     * @param fieldName
     * @param cls
     * @return java.lang.reflect.Method
     */
    public static Method getGetterMethodByField(String fieldName, Class cls){
        if ( Str.isNull(fieldName) ) return null;
        //先查getter
        Method getterMethod = null;
        //首字母大写
        String methodSuffix = Str.upperCaseConvert(fieldName, 0);
        try {
            getterMethod = cls.getMethod("get" + methodSuffix);
            if ( isJavaBeanGetterMethod(getterMethod) ) return getterMethod;

            throw new NoSuchMethodException("");
        } catch (NoSuchMethodException e) {
            //查is
            try {
                getterMethod = cls.getMethod("is" + methodSuffix);
                if ( isJavaBeanGetterMethod(getterMethod) ){
                    return getterMethod;
                }
            } catch (NoSuchMethodException ex) {
                return null;
            }
        }
        return null;
    }
    /**
     * @description:查询orm setter方法，通过属性名
     * @param fieldName
     * @param cls
     * @return java.lang.reflect.Method
     */
    public static Method getSetterMethodByField(String fieldName, Class cls){
        if ( Str.isNull(fieldName) ) return null;
        try {
            //找属性，获取属性类型
            Field field = cls.getDeclaredField(fieldName);
            //首字母大写
            String methodSuffix = Str.upperCaseConvert(fieldName, 0);
            Method setterMethod = cls.getMethod("set" + methodSuffix, field.getType());
            if ( isJavaBeanSetterMethod(setterMethod) ) return setterMethod;
        } catch (NoSuchFieldException | NoSuchMethodException e) {}
        return null;
    }
}