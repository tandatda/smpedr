package com.lauor.smpedr.core.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Function:非orm属性，sql语句将过滤它
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@InsertIgnore
@UpdateIgnore
public @interface SqlIgnore {
    String value() default "";
}
