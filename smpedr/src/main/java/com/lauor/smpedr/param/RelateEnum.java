package com.lauor.smpedr.param;

/**
 * @function:sql连接符
 */
public enum RelateEnum {
    AND("and"), OR("or");

    RelateEnum(String name) {
        this.name = name;
    }
    private String name;

    public String getName() {
        return name;
    }

    public String getBlankName(){
        return " " + name + " ";
    }
}
