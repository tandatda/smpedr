package com.lauor.smpedr.param;

import com.lauor.smpedr.utils.Str;

/**
 *
 */
public class SqlParam implements Cloneable{

    private int start = -1;

    private int pageSize;

    /** example: id desc */
    private String orderBy;

    private String groupBy;

    private String having;

    private int limit;

    public SqlParam() {}

    public SqlParam(int limit) {
        this.limit = limit;
    }

    public SqlParam(int start, int pageSize) {
        if (start < 0 || pageSize < 1){
            throw new IllegalArgumentException("start must great than -1 and pageSize must great than 0");
        }
        this.start = start;
        this.pageSize = pageSize;
    }

    public SqlParam(int start, int pageSize, String orderBy) {
        this(start, pageSize);
        this.orderBy = orderBy;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        if (start < 0){
            throw new IllegalArgumentException("start must great than -1");
        }
        this.start = start;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        if (pageSize < 1){
            throw new IllegalArgumentException("pageSize must great than 0");
        }
        this.pageSize = pageSize;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(String groupBy) {
        this.groupBy = groupBy;
    }

    public String getHaving() {
        return having;
    }

    public void setHaving(String having) {
        this.having = having;
    }

    public static boolean allNull(SqlParam param){
        return param == null || param.getLimit() < 1 && (param.getStart() < 0 || param.getPageSize() < 1)
                && Str.isNull( param.getOrderBy() ) && Str.isNull( param.getGroupBy() );
    }

    public boolean hasLimit(){
        return this.limit > 0 || this.start > -1 && this.pageSize > 0;
    }

    public SqlParam clone() {
        try {
            return (SqlParam) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new UnsupportedOperationException(e);
        }
    }
}
