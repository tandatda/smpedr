package com.lauor.smpedr.param;

/**
 * @author 13949
 * @function:sql参数比较符
 */
public enum OptEnum {
    EQ("="), NOT_EQ("!="), LT("<"), GT(">"), LT_EQ("<=")
    ,GT_EQ(">="), IN("in"), NOT_IN("not in"),
    LIKE("like");
    private String name;

    OptEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getBlankName() {
        return " "  + name + " ";
    }

    /** 操作符对应参数是否为集合 */
    public boolean isParamCollection(OptEnum opt){
        return opt == IN || opt == NOT_IN;
    }

    public String buildLikeParam(String param){
        return "%" + param + "%";
    }

    public String buildLeftLikeParam(String param){
        return "%" + param;
    }

    public String buildRightLiKeParam(String param){
        return param + "%";
    }
}
