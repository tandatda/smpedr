package com.lauor.smpedr.param;

import java.util.LinkedHashMap;

/**
 * @author 13949
 * @function:sql查询参数对象
 */
public class SqlArgMap<T> extends LinkedHashMap<String, SqlArgMap.ArgNode<T>> {
    public static class ArgNode<T>{
        private T value;
        private OptEnum opt;
        private RelateEnum relateOpt;

        public ArgNode(T value, OptEnum opt) {
            this.value = value;
            this.opt = opt;
            this.relateOpt = RelateEnum.AND;
        }

        public ArgNode(T value, OptEnum opt, RelateEnum relateOpt) {
            this.value = value;
            this.opt = opt;
            this.relateOpt = relateOpt;
        }

        public T getValue() {
            return value;
        }

        public OptEnum getOpt() {
            return opt;
        }

        public RelateEnum getRelateOpt() {
            return relateOpt;
        }
    }

    public SqlArgMap(int initSize) {
        super(initSize);
    }

    public SqlArgMap() {
        super(16);
    }

    public SqlArgMap put(String key, T value){
        super.put(key, new ArgNode(value, OptEnum.EQ));
        return this;
    }

    public SqlArgMap put(String key, OptEnum cmd, T value){
        super.put(key, new ArgNode(value, cmd));
        return this;
    }

    public SqlArgMap putIfNotNull(String key, OptEnum cmd, T value){
        if (value == null) {
            return this;
        }
        return put(key, cmd, value);
    }

    public SqlArgMap putIfNotNull(String key, T value){
        if (value == null) {
            return this;
        }

        return this.put(key, value);
    }

    public SqlArgMap putIfNotEmpty(String key, OptEnum cmd, T value){
        if (value == null || "".equals( value.toString() )){
            return this;
        }
        return put(key, cmd, value);
    }

    public SqlArgMap putIfNotEmpty(String key, T value){
        return putIfNotEmpty(key, OptEnum.EQ, value);
    }

    public SqlArgMap and(String key, T value){
        return this.and(key, OptEnum.EQ, value);
    }

    public SqlArgMap and(String key, OptEnum cmd, T value){
        super.put(key, new ArgNode(value, cmd, RelateEnum.AND));
        return this;
    }

    public SqlArgMap or(String key, OptEnum cmd, T value){
        super.put(key, new ArgNode(value, cmd, RelateEnum.OR));
        return this;
    }

    public SqlArgMap or(String key, T value){
        return this.or(key, OptEnum.EQ, value);
    }

    public ArgNode<T> getValue(String key){
        return super.get(key);
    }

    public T get(String key){
        ArgNode<T> argNode = super.get(key);
        if (argNode == null) {
            return null;
        }
        return argNode.getValue();
    }
}
