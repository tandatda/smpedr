package com.lauor.smpedr.session;

import com.lauor.smpedr.Configuration;
import com.lauor.smpedr.param.SqlArgMap;
import com.lauor.smpedr.param.SqlParam;

import java.util.List;

/**
 * 数据操作接口
 */
public interface SqlSession {
    /**
     * @description:查询一条数据
     * @param cls 数据表，返回结果对应实体类
     * @param sqlArgs nullable
     * @return T
     */
    <E> E selectOne(Class<E> cls, SqlArgMap sqlArgs);
    /**
     * @description:查询一条数据
     * @param cls 数据表，返回结果对应实体类
     * @param sqlArgs nullable
     * @param sqlParam nullable
     * @return T
     */
    <E> E selectOne(Class<E> cls, SqlArgMap sqlArgs, SqlParam sqlParam);
    /**
     * @description:查询一条数据
     * @param cls 返回结果对应实体类
     * @param tableName 数据表名
     * @param sqlArgs nullable
     * @param sqlParam nullable
     * @return T
     */
    <E> E selectOne(String tableName, Class<E> cls, SqlArgMap sqlArgs, SqlParam sqlParam);
    /**
     * @description:查询一条数据
     * @param cls 返回结果对应实体类
     * @param sql sql语句
     * @param sqlArgs nullable
     * @return T
     */
    <E> E selectOne(String sql, Class<E> cls, SqlArgMap sqlArgs);
    /**
     * @description:查询多条数据
     * @param cls
     * @param sqlArgs nullable
     * @param sqlParam nullable
     * @return java.util.List<T>
     */
    <E> List<E> selectList(Class<E> cls, SqlArgMap sqlArgs, SqlParam sqlParam);

    <E> List<E> selectList(Class<E> cls, SqlArgMap sqlArgs);
    /**
     * @description:查询多条数据
     * @param tableName 数据表名
     * @param cls 返回结果对应实体类
     * @param sqlArgs nullable
     * @param sqlParam nullable
     * @return java.util.List<T>
     */
    <E> List<E> selectList(String tableName, Class<E> cls, SqlArgMap sqlArgs, SqlParam sqlParam);
    /**
     * @description:查询多条数据
     * @param sql sql语句
     * @param cls 返回结果对应实体类
     * @param sqlArgs nullable
     * @return java.util.List<T>
     */
    <E> List<E> selectList(String sql, Class<E> cls, SqlArgMap sqlArgs);
    /**
     * @description:执行数据库更新操作,如delete,update,replace...
     * @param sql not null
     * @param dataList not null
     * @exception RuntimeException
     * @return int
     */
    <E> int executeUpdate(String sql, List<E> dataList);
    /**
     * @description:插入一条数据
     * @param cls 表名对应类
     * @param data
     * @param insertNull 属性为null是否插入，默认true
     * @return int The number of rows affected by the insert.
     */
    <E> int insert(Class<E> cls, E data, boolean insertNull);

    <E> int insert(Class<E> cls, E data);
    /**
     * @description:插入一条数据
     * @param tableName
     * @param data
     * @param insertNull 属性为null是否插入，默认true
     * @return int The number of rows affected by the insert.
     */
    <E> int insert(String tableName, E data, boolean insertNull);

    <E> int insert(String tableName, E data);
    /**
     * @description:插入多条数据
     * @param cls
     * @param dataList
     * @param insertNull 属性为null是否插入，默认true
     * @return int The number of rows affected by the insert.
     */
    <E> int batchInsert(Class cls, List<E> dataList, boolean insertNull);

    <E> int batchInsert(Class cls, List<E> dataList);
    /**
     * @description:插入多条数据
     * @param tableName 表名
     * @param dataList
     * @param insertNull 属性为null是否插入，默认true
     * @return int The number of rows affected by the insert.
     */
    <E> int batchInsert(String tableName, List<E> dataList, boolean insertNull);

    <E> int batchInsert(String tableName, List<E> dataList);
    /**
     * @description:更新操作
     * @param obj
     * @param sqlArgs
     * @return int
     */
    int update(Object obj, SqlArgMap sqlArgs);
    /**
     * @description:更新操作
     * @param obj
     * @param sqlArgs
     * @param updateNull 是否更新属性为空的字段，默认true
     * @return int
     */
    int update(Object obj, SqlArgMap sqlArgs, boolean updateNull);
    /**
     * @description:更新操作
     * @param obj
     * @param sqlArgs
     * @param updateNull 是否更新属性为空的字段，默认true
     * @param sqlParam
     * @return int
     */
    int update(Object obj, SqlArgMap sqlArgs, SqlParam sqlParam, boolean updateNull);
    /**
     * @description:更新操作
     * @param tableName 表名
     * @param obj
     * @param sqlArgs
     * @param updateNull 是否更新属性为空的字段，默认true
     * @return int
     */
    int update(String tableName, Object obj, SqlArgMap sqlArgs, boolean updateNull);
    /**
     * @description:更新操作
     * @param tableName 表名
     * @param obj
     * @param sqlArgs
     * @param sqlParam
     * @param updateNull 是否更新属性为空的字段，默认true
     * @return int
     */
    int update(String tableName, Object obj, SqlArgMap sqlArgs, SqlParam sqlParam, boolean updateNull);
    /**
     * @description:删除数据
     * @param cls
     * @param sqlArgs
     * @param sqlParam 默认null
     * @return int The number of rows affected by the delete.
     */
    int delete(Class cls, SqlArgMap sqlArgs, SqlParam sqlParam);
    /**
     * @description:删除数据
     * @param cls
     * @param sqlArgs
     * @return int The number of rows affected by the delete.
     */
    int delete(Class cls, SqlArgMap sqlArgs);
    /**
     * @description:删除数据
     * @param tableName
     * @param sqlArgs
     * @param sqlParam 默认null
     * @return int The number of rows affected by the delete.
     */
    int delete(String tableName, SqlArgMap sqlArgs, SqlParam sqlParam);
    /**
     * @description:删除数据
     * @param tableName
     * @param sqlArgs
     * @return int The number of rows affected by the delete.
     */
    int delete(String tableName, SqlArgMap sqlArgs);
    /**
     * @description:提交database connection
     * @param force 强制提交
     * @return void
     */
    void commit(boolean force);

    void commit();
    /**
     * @description:回滚database connection
     * @param force 强制回滚
     * @return void
     */
    void rollback(boolean force);

    void rollback();

    void close();
    /**
     * @description:连接是否已关闭
     * @return boolean
     */
    boolean isClosed();

    Configuration getConfiguration();
}