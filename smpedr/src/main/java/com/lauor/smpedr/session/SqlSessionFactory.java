package com.lauor.smpedr.session;

import com.lauor.smpedr.core.SqlGenerator;
import com.lauor.smpedr.transaction.TransactionIsolationLevel;
/**
 * 生成SqlSession
 */
public interface SqlSessionFactory {

    SqlSession openSession();
    /**
     * @description:获取sqlSession
     * @param autoCommit 是否自动提交
     * @return com.lauor.smpedr.api.SqlSession
     */
    SqlSession openSession(boolean autoCommit);
    /**
     * @description:获取sqlSession,指定隔离级别
     * @param level 数据库隔离级别
     * @return com.lauor.smpedr.api.SqlSession
     */
    SqlSession openSession(TransactionIsolationLevel level);
    /**
     * @description:获取sqlSession
     * @param level 数据库隔离级别
     * @param autoCommit 是否自动提交
     * @return com.lauor.smpedr.api.SqlSession
     */
    SqlSession openSession(TransactionIsolationLevel level, boolean autoCommit);
    /**
     * @description:获取所使用的Sql生成器
     * @return com.lauor.smpedr.core.SqlGenerator
     */
    SqlGenerator getSqlGenerator();
}
