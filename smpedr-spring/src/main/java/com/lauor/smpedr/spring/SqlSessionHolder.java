package com.lauor.smpedr.spring;


import com.lauor.smpedr.session.SqlSession;
import org.springframework.transaction.support.ResourceHolderSupport;
import org.springframework.util.Assert;

public class SqlSessionHolder extends ResourceHolderSupport {
    private final SqlSession sqlSession;

    public SqlSessionHolder(SqlSession sqlSession) {
        Assert.notNull(sqlSession, "No SqlSession specified");
        this.sqlSession = sqlSession;
    }

    public SqlSession getSqlSession() {
        return sqlSession;
    }
}
