package com.lauor.smpedr.spring.transaction;

import com.lauor.smpedr.transaction.Transaction;
import com.lauor.smpedr.transaction.TransactionFactory;
import com.lauor.smpedr.transaction.TransactionIsolationLevel;

import javax.sql.DataSource;
import java.sql.Connection;

/**
 * spring 管理事务对象工厂
 */
public class SpringManagedTransactionFactory implements TransactionFactory {

    public Transaction newTransaction(Connection connection) {
        throw new UnsupportedOperationException("creating Spring transactions require a DataSource");
    }

    public Transaction newTransaction(DataSource dataSource, TransactionIsolationLevel level, boolean autoCommit) {
        return new SpringManagedTransaction(dataSource);
    }
}
