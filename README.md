# smpedr

#### 介绍
简洁快速稳定的持久层框架，目前只实现mysql.

### JMeter压测报告
1.  测试机器性能参数  
处理器: Intel(R) Core(TM) i7-8550U CPU @ 1.8GHZ  
内存: 8GB  
硬盘: 固态硬盘/嵌入式多媒体控制器 (EMMC) 1 238GB, Micron_1100_MTFDDAV256TBN  
系统:windows10家庭中文版
2.  测试样例  
一分钟1200次查询单表前10条数据，排序，总记84万三千次请求。
3.  测试结果
![image](https://gitee.com/tandatda/smpedr/raw/03358bc2b909ebe20b622efe56d58a4eb97fee58/%E5%8E%8B%E6%B5%8B%E6%8A%A5%E5%91%8A.JPG)


#### 安装教程

1.  clone到本地
2.  根目录(edr)运行 mvn clean install
3.  添加依赖到项目

#### 使用说明
1.  java最低版本:1.8

2.  工程依赖  
非spring:
```xml
    <dependency>  
        <groupId>com.lauor</groupId>  
        <artifactId>smpedr-mysql</artifactId>  
        <version>${version}</version>  
    </dependency>  
```
    
spring mvc:
```xml
    <dependency>
        <groupId>com.lauor</groupId>
        <artifactId>smpedr-mysql</artifactId>
        <version>${version}</version>
    </dependency>
    <dependency>  
        <groupId>com.lauor</groupId>  
        <artifactId>smpedr-spring</artifactId>  
        <version>${version}</version>  
    </dependency>
```
    
spring boot:
```xml
    <dependency>  
        <groupId>com.lauor</groupId>  
        <artifactId>smpedr-mysql-spring-boot-starter</artifactId>  
        <version>${version}</version>  
    </dependency>
```
    
#### 基本使用说明
非spring工程  
```
//构建SqlSessionFactory  
    SqlSessionFactory buildSqlSessionFactory(DataSource dataSource){  
        Configuration configuration = new Configuration(dataSource, new JdbcTransactionFactory());  
        //添加拦截器，可选  
        Interceptor interceptor = Interceptor instance;  
        configuration.addInterceptor(interceptor);  
        return new DefaultSqlSessionFactory(configuration, new SqlGeneratorMysql());  
    }  
//打开sqlSession操作数据库  
    SqlSession sqlSession = sqlSessionFactory.openSession();  
    sqlSession.selectList(User.class, null, new SqlParam(0, 10));  
    sqlSession.close();
 ```
spring mvc工程
```
//构建SqlSessionFactory  
    @Bean  
    public SqlSessionFactory buildSqlSessionFactory(DataSource dataSource, Environment environment){  
        Configuration configuration = new Configuration(dataSource, new SpringManagedTransactionFactory());  
        //添加拦截器，可选  
        Interceptor interceptor = Interceptor instance;  
        configuration.addInterceptor(interceptor);  
        return new SpringManagedSqlSessionFactory(configuration, new SqlGeneratorMysql());  
    }  
//打开sqlSession操作数据库
    @Autowired
    SqlSessionFactory sqlSessionFactory;

    SqlSession sqlSession = sqlSessionFactory.openSession();  
    sqlSession.selectList(User.class, null, new SqlParam(0, 10)); 
```  
spring boot工程  
```
//直接打开sqlSession操作数据库
    @Autowired
    SqlSessionFactory sqlSessionFactory;

    SqlSession sqlSession = sqlSessionFactory.openSession();  
    sqlSession.selectList(User.class, null, new SqlParam(0, 10)); 
``` 
    
#### 详细demo
https://gitee.com/tandatda/demo-edr-smpdb.git
